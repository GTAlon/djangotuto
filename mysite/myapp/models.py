from django.db import models

# Create your models here.

class Book(models.Model):
    title = models.CharField(max_length=256,blank=False,default='')
    autor = models.CharField(max_length=256,blank=False,default='')
    year = models.PositiveIntegerField(default=0,null=True,blank=True)
    price = models.FloatField(default=-1.0,null=True,blank=True)
    description = models.TextField(max_length=256,blank=False,default='')
    bestseller = models.BooleanField(default=False)