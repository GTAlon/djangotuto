from django.shortcuts import render
from django.http import HttpResponse,JsonResponse
from rest_framework.views import APIView
from rest_framework import status
from .models import Book
from .serializers import BookSerializer
# Create your views here.

def index(request):
    return HttpResponse("Hello world, this ma index")

class Booklist(APIView):
    def get(self,request,format=None):
        books = Book.objects.all()
        serializer = BookSerializer(books,many=True)
        return JsonResponse(serializer.data,safe=False)
    
    def post(self,request,format=None):
        serializer=BookSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data,safe=False,status= status.HTTP_201_CREATED)
        return JsonResponse(serializer.errors,safe=False,status=status.HTTP_400_BAD_REQUEST)


class BookById(APIView):
    def get(self,request,id):
        book = Book.objects.get(id=id)
        serializer = BookSerializer(book)
        return JsonResponse(serializer.data)

    def put(self,request,id):
        book = Book.objects.get(id=id)
        serializer=BookSerializer(book,data=request.data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors,status=status.HTTP_400_BAD_REQUEST)
    
    def delete(self,request,id):
        book = Book.objects.get(id=id)
        book.delete()
        return JsonResponse({'Message' : 'Le livre a bien été supprimé.'})