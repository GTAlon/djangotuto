from django.contrib import admin
from django.urls import path,include
from . import views

urlpatterns = [
    path('',views.index,name="index"),
    #books
    path("books/",views.Booklist.as_view(),name="book-list"),
    path('book/<int:id>/', views.BookById.as_view(),name="book-id"),
]